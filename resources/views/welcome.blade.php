@extends('layouts.app')
@include('welcome.slider')
@include('welcome.portfolio')
@section('welcome')
@yield('slider')
@yield('portfolio')    
@endsection